package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	//"golang.org/x/net/websocket"
	"log"
	"time"
	"net/http"
)

func main() {
	url := "ws://localhost:8080/ws"  //服务器地址
	h := http.Header{}
	h.Set("Sec-WebSocket-Protocol", "hey")
	ws, _, err := websocket.DefaultDialer.Dial(url, h)
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		for {
			err := ws.WriteMessage(websocket.BinaryMessage,[]byte("ping"))
			if err != nil {
				log.Fatal(err)
			}
			time.Sleep(time.Second*2)
		}
	}()

	for {
		_, data, err := ws.ReadMessage()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("receive: ", string(data))
	}
}
